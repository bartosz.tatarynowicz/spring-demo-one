package com.luv2code.springdemo;

import java.util.Random;

public class GreatFortuneService implements FortuneService {

	@Override
	public String getFortune() {
		String[] fortuneTable = new String[3];
		fortuneTable[0] = "Great day to die.";
		fortuneTable[1] = "It was an honor, sir.";
		fortuneTable[2] = "Keep going.";
		Random generator = new Random();
		return fortuneTable[generator.nextInt(3)];
	}

}
